const express = require('express');
const path = require('path');
const logger = require('morgan');
const cors = require('cors');

const chatRouter = require('./routes/chatRouter');

const corsOptions = {
    origin: ['http://localhost:5500', 'http://127.0.0.1:5501']
}

const app = express();

app.use(cors(corsOptions));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/messages', chatRouter);

module.exports = app;
