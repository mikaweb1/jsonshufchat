const express = require('express');
const chatRepo = require('../repositories/chatRepo');

// insert,
// deleteMessage

const getAll = () => chatRepo.getAll();

const getAllBySender = (sender) => chatRepo.getAllBySender(sender);

const getAllByReciver = (reciver) => chatRepo.getAllByReciver(reciver);

const getAllByUser = (user) => {
    const userMessages = chatRepo.getAllBySender(user);
    (chatRepo.getAllByReciver(user)).forEach(element => {        
        userMessages.push(element);
    });

    return userMessages;
}

const getByChat = (sender, reciver) => {
    if (chatRepo.sortChat(reciver)) {

        return chatRepo.getByGroupChat(reciver);
    } else {
        
        return chatRepo.getByDirectChat(sender, reciver);
    }
}

const insert = (messageObject) => {
    //takes data from router and returns it to repo
    
    return chatRepo.insert(messageObject);
}


const deleteMessage = () => {
    //tells which message to delete
}

module.exports = {
    getAll,
    getAllBySender,
    getAllByReciver,
    getAllByUser,
    getByChat,
    insert
    // deleteMessage
}