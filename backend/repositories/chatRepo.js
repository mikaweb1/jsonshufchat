const express = require('express');
const fs = require('fs');
// path from app.js "./data/messages.json" 
// DATA IN JS FORM (PARSE)
const data = JSON.parse(fs.readFileSync("./data/messages.json", "utf8"));
const { messages } = data;
const groupList = require("../data/groups.json");

const getAll = () => messages;

const getAllBySender = (sender) => {

    //return all messages sent to and from $USER  
    return messages.filter(element => element.sender === sender);
}

const getAllByReciver = (reciver) => {

    // return all messages sent to and from $USER  
    return messages.filter(element => element.reciver === reciver);
}

const getByGroupChat = (reciver) => messages.filter(element => element.reciver === reciver);


const getByDirectChat = (sender, reciver) => {

    return messages.filter(element =>
        element.reciver === reciver && element.sender === sender
        || element.sender === reciver && element.reciver === sender);
}

const sortChat = (reciver) => {
    if (groupList.groups.filter(
        element => element.name === reciver).length !== 0) {

        return true;
    } else {

        return false;
    }
}

const insert = (messageObject) => {
    messages.push(messageObject);
    
    const dataToJson = JSON.stringify({ messages});
    fs.writeFileSync('./data/messages.json', dataToJson, "utf8");
}

const deleteMessage = () => {
    //takes info from user on by event and removes matching query from db
}

module.exports = {
    getAll,
    getAllBySender,
    getAllByReciver,
    sortChat,
    getByGroupChat,
    getByDirectChat,
    insert,
    deleteMessage
};