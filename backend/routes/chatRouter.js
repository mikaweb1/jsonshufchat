const express = require('express');
const chatService = require('../services/chatService');
const router = express.Router();

// TO DO (FRONTEND)

// initial state: get all messages 
router.get('/', (req, res) => {
    res.send(chatService.getAll());

});

// empty chat-input: get all from/by sender 
router.get('/:user', (req, res) => {
    res.status(200).json(chatService.getAllByUser(req.params.user));
})

// chat chosen: show message flow of chat
router.get('/:user/chats/:chat', (req, res) => {
    res.status(200).send(chatService.getByChat(req.params.user, req.params.chat));
});

// sent button clicked: show only sent
router.get('/:user/sent', (req, res) => {
    res.status(200).json(chatService.getAllBySender(req.params.user));
});

// recived button clicked: show only recived
router.get('/:user/recived', (req, res) => {
    res.status(200).json(chatService.getAllByReciver(req.params.user));
});

router.post('/post', (req, res) => {
    console.log(req.body);
    chatService.insert(req.body);
    res.send('data was recived by server');
});

module.exports = router; 