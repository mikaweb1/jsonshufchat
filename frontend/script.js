
// Get necessary elements
const messageInput = document.getElementById('message-input');
const senderInput = document.getElementById('sender-input-box');
const chatInput = document.getElementById('chat-input-box');
const sendButton = document.getElementById('send-button');
const chatMessages = document.querySelector('.chat-messages');
const refreshButton = document.getElementById('refresh-button');
const senderOfMessages = document.querySelector('.sender');
const baseurl = "http://localhost:5500";

// Function to create a new message element
function createMessageElement(sender, content, date) {
  const messageElement = document.createElement('div');
  if (senderInput.value.trim() === sender) {
    messageElement.classList.add('right-message');
  } else {
    messageElement.classList.add('message');
  }

  const senderElement = document.createElement('div');
  senderElement.classList.add('sender');
  senderElement.textContent = sender;

  const contentElement = document.createElement('div');
  contentElement.classList.add('message-content');
  contentElement.textContent = content;

  const dateElement = document.createElement('div');
  dateElement.classList.add('date');
  dateElement.textContent = date;

  messageElement.appendChild(senderElement);
  messageElement.appendChild(contentElement);
  messageElement.appendChild(dateElement);

  return messageElement;
}

const postToServer = async (sender, message, timestamp, date) => {
  // INSERT to db
  const chat = chatInput.value.trim();
  const messageData = {
    sender,
    "messageContent": message,
    "reciver": chat,
    timestamp,
    date
  }

   const res = await fetch(`${baseurl}/messages/post`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(messageData)
  });
  console.log(await res.json());
}

// Function to handle sending a message
 function sendMessage() {
  const message = messageInput.value.trim(); 
  const sender = senderInput.value.trim(); 

  // get time in HH:MM
  const today = new Date();
  const date = today.getHours() + ":" + ((today.getMinutes()<10?'0':'') + today.getMinutes());
  const timestamp = Date.now(); 

  if (message !== '') {
    const messageElement = createMessageElement(sender, message, date);
    chatMessages.appendChild(messageElement);

    postToServer(sender, message, timestamp, date);

    // Clear the input field
    messageInput.value = '';
  }
}

const getAllMessages = async () => {
  const res = await fetch(`${baseurl}/messages`);

  if (res.ok) {

    return await res.json();
  } else {
    console.error("error getting response from server");
  }
}

// display user messages on chat-container
const getUserMessages = async (sender) => {
  const res = await fetch(`${baseurl}/messages/${sender}`);

  if (res.ok) {

    return await res.json();
  } else {
    console.error("error getting response from server");
  }
}

// display chat messages
const getChatMessages = async (sender, chat) => {
  const res = await fetch(`${baseurl}/messages/${sender}/chats/${chat}`);

  if (res.ok) {

    return await res.json();
  } else {
    console.error("error getting response from server");
  }
}

// sort what data type to show
const getData = async (sender, chat) => {
  if (sender == '') {
    return await getAllMessages
      ();
  } else if (chat == '') {
    return await getUserMessages(sender);
  } else {
    return await getChatMessages(sender, chat);
  }
}

// BUTCH
// const clearChatContainer = async () => {
//   const toRemove = await document.querySelector(".chatMessages");
//   console.log(chatMessages);

//   toRemove.forEach(element =>
//     element.remove(element));
// }

const existingMessages = async () => {
  const sender = senderInput.value.trim();
  const chat = chatInput.value.trim();
  const data = await getData(sender, chat);
  (await data).forEach(element => {
    const messageElement = createMessageElement(element.sender, element.messageContent, element.date);
    chatMessages.appendChild(messageElement);
  });
}

// Event listener for the refresh button click
refreshButton.addEventListener('click', existingMessages);

// Event listener for the send button click
sendButton.addEventListener('click', sendMessage);

// Event listener for the Enter key press in the input field
messageInput.addEventListener('keypress', function (event) {
  if (event.key === 'Enter') {
    sendMessage();
  }
});