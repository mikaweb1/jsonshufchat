
// Get necessary elements
const messageInput = document.getElementById('message-input');
const senderInput = document.getElementById('sender-input-box');
const chatInput = document.getElementById('chat-input-box');
const sendButton = document.getElementById('send-button');
const chatMessages = document.querySelector('.chat-messages');
const refreshButton = document.getElementById('refresh-button');
const senderOfMessages = document.querySelector('.sender');
const baseurl = "http://localhost:5500";

// Function to create a new message element
function createMessageElement(sender, content, date) {
  const messageElement = document.createElement('div');
  if (senderInput.value.trim() === sender) {
    messageElement.classList.add('right-message');
  } else {
    messageElement.classList.add('message');
  }

  const senderElement = document.createElement('div');
  senderElement.classList.add('sender');
  senderElement.textContent = sender;

  const contentElement = document.createElement('div');
  contentElement.classList.add('message-content');
  contentElement.textContent = content;

  const dateElement = document.createElement('div');
  dateElement.classList.add('date');
  dateElement.textContent = date;

  messageElement.appendChild(senderElement);
  messageElement.appendChild(contentElement);
  messageElement.appendChild(dateElement);

  return messageElement;
}

// Function to handle sending a message
function sendMessage() {
  const message = messageInput.value.trim(); //send to db
  const sender = senderInput.value.trim(); //send to db

  // get time in HH:MM
  const today = new Date();
  const date = today.getHours() + ":" + today.getMinutes();
  
  const timestamp = Date.now(); //send to db

  if (message !== '') {
    const messageElement = createMessageElement(sender, message, date);
    chatMessages.appendChild(messageElement);

    // Clear the input field
    messageInput.value = '';
  }
}

const getAll = async () => {
  const res = await fetch(`${baseurl}/messages`);

  if (res.ok) {
    
    return await res.json();
  } else {
    console.log("error getting response");
  }
}

// display user messages on chat-container
const getByUser = async (sender) => {
  const res = await fetch(`${baseurl}/messages/${sender}`);  

  if (res.ok) {
    
    return await res.json();
  } else {
    console.log("error getting response");
  }
}

// display chat messages
const getByChat = async (sender, chat) => {
  const res = await fetch(`${baseurl}/messages/${sender}/chats/${chat}`);

  if (res.ok) {
    
    return await res.json();
  } else {
    console.log("error getting response");
  }
}

// sort what data type to show
const getData = async (sender, chat) => {
  if (sender == '') {
    return await getAll();
  } else if (chat == '') {
    return await getByUser(sender);
  } else {
    return await getByChat(sender, chat);
  }
}

// BUTCH
// const clearChatContainer = async () => {
//   const toRemove = await document.querySelector(".chatMessages");
//   console.log(chatMessages);

//   toRemove.forEach(element =>
//     element.remove(element));
// }

const existingMessages = async () => {  
  const sender = senderInput.value.trim();
  const chat = chatInput.value.trim();
  const data = await getData(sender, chat);
  (await data).forEach(element => {
    const messageElement = createMessageElement(element.sender, element.messageContent, element.date);
    chatMessages.appendChild(messageElement);
  });
}

// Event listener for the refresh button click
refreshButton.addEventListener('click', existingMessages);

// Event listener for the send button click
sendButton.addEventListener('click', sendMessage);

// Event listener for the Enter key press in the input field
messageInput.addEventListener('keypress', function (event) {
  if (event.key === 'Enter') {
    sendMessage();
  }
});

